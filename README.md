# New World Colony Remake

Resources:

- [Website archive](https://web.archive.org/web/20140301122725/http://www.82apps.com/colony/)
- [Soundtrack](https://erikthomas82.bandcamp.com/album/new-world-colony-soundtrack), [archived soundtrack](https://archive.org/details/new-world-colony-ios-game-ost-by-erik-thomas)
- [Screen recording of tutorial (on youtube)](https://www.youtube.com/watch?v=McZwQaE3A5w)

Other videos:

- https://www.youtube.com/watch?v=c9qwOodRqyI
- https://www.youtube.com/watch?v=rRjHzSmpcsg
- https://www.youtube.com/watch?v=O8wHOekfh_w
- https://www.youtube.com/watch?v=V03JUWPkR5Y
- https://www.youtube.com/watch?v=cr6GPNlrUoo
